export class Formmodel {
  name:string;
  from:string;
  to:string;
  type:string;
  departure:Date;
  arrival:Date;
  adults:number;
  children:number;
  infants:number;
  constructor(name:string,from:string,to:string,type:string,departure:Date,arrival:Date,adults:number,children:number,infants:number){
    this.name =name;
    this.from = from;
    this.to = to;
    this.type=type;
    this.departure =departure;
    this.arrival = arrival;
    this.adults = adults;
    this.children = children;
    this.infants =infants
  }
}
