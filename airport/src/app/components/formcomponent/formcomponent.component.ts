import { Component, OnInit } from '@angular/core';
import { Formmodel } from './formmodel';
import {FormBuilder,FormGroup,Validators} from '@angular/forms'
import { PageService } from 'src/app/share/page.service';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'app-formcomponent',
  templateUrl: './formcomponent.component.html',
  styleUrls: ['./formcomponent.component.css']
})
export class FormcomponentComponent implements OnInit {
  Books:Array<Formmodel> = []
  Book!:Formmodel;
  Bookform!:FormGroup;
  selectedstart:any =""
  selectedend:any=""
  selectedtype:any ="One way"

  constructor(private fb:FormBuilder,private pageService:PageService) {
    this.Book = new Formmodel("","","","",new Date,new Date,0,0,0)
   }
   convertdate(f:Date){
    var datat = (f).toLocaleString("th-TH")
    datat = datat.split(" ")[0]
      return datat
   }
   submitb(f:FormGroup):void {
    console.log(this.Bookform.value);
    var date1 = f.get('Departure')?.value
    var date2 = f.get('Arrival')?.value
    var startplace = f.get('From')?.value
    var endplace = f.get('To')?.value
    if(date1>date2){
      alert("please enter date time correctly")
    }
    else if(startplace ===endplace){
      alert("please not enter same destination")
    }
    else if(f.get('Adult')?.value===0){
      alert("please enter at least 1 adult")
    }
    else if(f.get('Type')?.value===""){
      alert("please select trip type")
    }
    else {

      this.Book.name = f.get('Name')?.value;
      this.Book.from = f.get('From')?.value;
      this.Book.to = f.get('To')?.value;
      this.Book.type = f.get('Type')?.value;
      this.Book.departure = new Date(f.get('Departure')?.value);
      this.Book.arrival = new Date(f.get('Arrival')?.value);
      this.Book.adults = f.get('Adult')?.value;
      this.Book.children = f.get('Children')?.value;
      this.Book.infants = f.get('Infant')?.value;
      let form_record = new Formmodel(f.get('Name')?.value,
        f.get('From')?.value
        , f.get('To')?.value
        , f.get('Type')?.value,
        f.get('Departure')?.value,
        f.get('Arrival')?.value,
        f.get('Adult')?.value,
        f.get('Children')?.value,
        f.get('Infant')?.value
      )
      this.Books.push(form_record)
    }

  }
  ngOnInit(): void {
    this.Bookform = this.fb.group({
      Name:['',Validators.required],
      From:[this.selectedstart,Validators.required],
      To:[this.selectedend,Validators.required],
      Type:this.selectedtype,
      Departure:[''],
      Arrival:[''],
      Adult:[null,[Validators.required,Validators.min(0),Validators.max(50)]],
      Children:[null,[Validators.required,Validators.min(0),Validators.max(50)]],
      Infant:[null,[Validators.required,Validators.min(0),Validators.max(50)]],

    })
    this.Books = this.pageService.getData()
  }

}
