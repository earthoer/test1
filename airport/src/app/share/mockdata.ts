import { Formmodel } from "../components/formcomponent/formmodel";
export class Mockdata {
  public static mdata:Formmodel[]=[
    {
      name:"job",
      from:"Thailand",
      to:"England",
      type:"return",
      departure:new Date,
      arrival:new Date,
      adults:2,
      children:0,
      infants:0,


    },
    {
      name:"joey",
      from:"England",
      to:"Thailand",
      type:"return",
      departure:new Date,
      arrival:new Date,
      adults:2,
      children:0,
      infants:1,


    },
  ]
}
