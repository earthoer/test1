import { Injectable } from '@angular/core';
import { Formmodel } from '../components/formcomponent/formmodel';
import { Mockdata } from './mockdata';
@Injectable({
  providedIn: 'root'
})
export class PageService {
  Books:Formmodel[]=[]
  constructor() {
    this.Books = Mockdata.mdata;
  }
  getData():Formmodel[]{
    return this.Books;
  }
  addData(d:Formmodel){
    this.Books.push(d)
  }
}
